(ns hwo2014bot.util-test
  (:use clojure.test
        hwo2014bot.util))

(deftest near=-test
  (testing "integers"
    (is (near= 1 1))
    (is (not (near= 1 2))))
  (testing "doubles"
    (is (near= 1.0 1.00000005))
    (is (not (near= 1.0 1.0000005))))
  (testing "collections of doubles"
    (is (near= [1.0 2.0] [1.0 2.00000005]))
    (is (not (near= [1.0 2.0] [1.0 2.0000005])))
    (is (not (near= [1.0 2.0] [1.0 2.0 3.0]))))
  (testing "maps of doubles"
    (is (near= {:foo 1.0 :bar 2.0} {:foo 1.0 :bar 2.00000005}))
    (is (not (near= {:foo 1.0 :bar 2.0} {:foo 1.0 :bar 2.0000005})))
    (is (not (near= {:foo 1.0 :bar 2.0} {:foo 1.0 :bar 2.0 :baz 3.0}))))
  (testing "empty collections"
    (is (near= [] []))
    (is (near= nil nil))
    (is (near= {} {}))
    (is (not (near= [1] [])))
    (is (not (near= [] [1])))
    (is (not (near= {:foo 1} {})))
    (is (not (near= {} {:foo 1}))))
  (testing "other objects"
    (is (near= "foo" "foo"))
    (is (not (near= "foo" "bar")))
    (is (not (near= 1.0 nil)))
    (is (not (near= nil 1.0)))
    (is (not (near= [{:foo "bar"}] {:foo "bar"})))
    (is (not (near= {:foo "bar"} [{:foo "bar"}])))))

(deftest mode-test
  (is (= 1 (mode [1 1 2])))
  (is (= 2 (mode [2 1 2])))
  (is (= 2 (mode [1 2 2])))
  (is (= 1.0 (mode [1.0001 1.0 1.0 1.0]))))
