(ns hwo2014bot.core-test
  (:use clojure.test
        hwo2014bot.core
        hwo2014bot.util)
  (:require [clojure.pprint :refer [pprint]]))

(def me {:name "Kaali", :color "red"})
(def keimola {:id            "keimola",
              :name          "Keimola",
              :pieces        [{:length 100.0} {:length 100.0} {:length 100.0} {:length 100.0, :switch true} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 200, :angle 22.5, :switch true} {:length 100.0} {:length 100.0} {:radius 200, :angle -22.5} {:length 100.0} {:length 100.0, :switch true} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:length 100.0, :switch true} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 200, :angle 22.5} {:radius 200, :angle -22.5} {:length 100.0, :switch true} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:length 62.0} {:radius 100, :angle -45.0, :switch true} {:radius 100, :angle -45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:length 100.0, :switch true} {:length 100.0} {:length 100.0} {:length 100.0} {:length 90.0}],
              :lanes         [{:distanceFromCenter -10, :index 0}
                              {:distanceFromCenter 10, :index 1}],
              :startingPoint {:position {:x -300.0, :y -44.0},
                              :angle    90.0}})

(defn quick-race [track]
  (-> (make-world)
      (update-world {:msgType "yourCar", :data me})
      (update-world {:msgType "gameInit",
                     :data    {:race {:track       track,
                                      :cars        [{:id me, :dimensions {:length 40.0, :width 20.0, :guideFlagPosition 10.0}}],
                                      :raceSession {:laps 3, :maxLapTimeMs 60000, :quickRace true}}}})))

(def keimola-race (quick-race keimola))

(defn custom-track-race [pieces]
  (quick-race (assoc keimola :pieces pieces)))

(deftest simulate-world-test
  (let [world (-> keimola-race
                  (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.0, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e"})
                  (update-world {:msgType "gameStart", :data nil, :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 0})
                  (update-world {:gameTick 0, :data 1.0, :msgType "throttle"})
                  (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.21419526611849038, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 1})
                  (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.6380559337212726, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 2}))]
    (testing "acceleration"
      (is (near= [{:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 1.267147937086869, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameTick 3}]
                 (simulate-server-response world))))
    (testing "multiple ticks"
      (let [world (simulate-world world 10)]
        (is (= 12 (:gameTick world)))
        (is (near= 2.2914781382898646 (:velocity (first (car-history world)))))))
    (testing "angle"
      (let [world (-> world
                      (update-world {:gameTick 81, :msgType "throttle", :data 0.6417691610948363})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 4, :inPieceDistance 6.398772082013352, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 82})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.2935624526866469, :piecePosition {:pieceIndex 4, :inPieceDistance 12.898772082013352, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 83})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.8489459178631971, :piecePosition {:pieceIndex 4, :inPieceDistance 19.398772082013352, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 84})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 1.6354558036261007, :piecePosition {:pieceIndex 4, :inPieceDistance 25.898772082013352, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 85}))]
        (is (near= [{:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 2.6235890750948987, :piecePosition {:pieceIndex 4, :inPieceDistance 32.39877208201335, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameTick 86}]
                   (simulate-server-response world)))))
    (testing "piece"
      (let [world (-> world
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 91.86324688287628, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 32})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 96.98893963775305, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 33}))]
        (is (near= [{:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 1, :inPieceDistance 2.220427997276616, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameTick 34}]
                   (simulate-server-response world)))))
    (testing "lap"
      (let [world (-> world
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 39, :inPieceDistance 73.58972249523673, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 3029})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 39, :inPieceDistance 82.14070874999271, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 3030}))]
        (is (near= [{:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.7250514926585652, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 1}}], :gameTick 3031}]
                   (simulate-server-response world)))))
    (testing "crash"
      (let [world (-> world
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 56.12940953563462, :piecePosition {:pieceIndex 6, :inPieceDistance 5.0701193010912675, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 106})
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 59.63801391198608, :piecePosition {:pieceIndex 6, :inPieceDistance 12.931278710761841, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "18dcc5b5-6ee0-4ce2-9fff-8263a9c00d1e", :gameTick 107}))]
        (is (near= [{:msgType "crash", :data {:name "Kaali", :color "red"}, :gameTick 108}
                    {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 6, :inPieceDistance 12.931278710761841, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameTick 108}]
                   (simulate-server-response world)))))))

(deftest advance-track-test
  (testing "will consume the velocity's worth of track"
    (is (= [{:length 15.0}]
           (advance-track 5.0 [{:length 20.0}]))))
  (testing "will remove the first piece after it's fully consumed"
    (is (= [{:length 100.0}]
           (advance-track 5.0 [{:length 5.0} {:length 100.0}]))))
  (testing "will consume also the following pieces if the first was not long enough"
    (is (= [{:length 97.0}]
           (advance-track 5.0 [{:length 2.0} {:length 100.0}])))))

(deftest predict-throttles-test
  (let [world (make-world)]
    (testing "keeps at speed limit after reaching it"
      (is (near= [0.4 0.4 0.4 0.4 0.4]
                 (predict-throttles world 5 4.0 [{:length 100.0, :speed-limit 4.0}]))))
    (testing "speed up on reaching the fast speed area"
      ; TODO: reaching the next piece in the middle of a tick - increase speed one tick earlier?
      (is (near= [0.4 0.4 1.0 0.812 0.42]
                 (predict-throttles world 5 4.0 [{:length 8.0, :speed-limit 4.0}
                                                 {:length 100.0, :speed-limit 4.2}]))))
    (testing "slow down in advance before reaching the slow speed area"
      ; TODO
      #_(is (near= [0.5 0.0 0.0 0.0 0.4]
                 (predict-throttles world 5 5.0 [{:length 20.0, :speed-limit 5.0}
                                           {:length 100.0, :speed-limit 4.0}]))))))

(deftest predict-throttle-test
  (let [world (make-world)
        speed-limit 5.0]
    (testing "full throttle when well below the speed limit"
      (is (= 1.0 (predict-throttle world 0.0 speed-limit))))
    (testing "no throttle when well over the speed limit"
      (is (= 0.0 (predict-throttle world 10.0 speed-limit))))
    (testing "keep at speed limit when it has been reached"
      (is (= 0.5 (predict-throttle world 5.0 speed-limit))))
    (testing "slowly increase throttle when just a little under the speed limit"
      (is (near= 0.745 (predict-throttle world 4.95 speed-limit))))
    (testing "slowly decrease throttle when just a little over the speed limit"
      (is (near= 0.255 (predict-throttle world 5.05 speed-limit))))))

(deftest predict-velocity-test
  (let [world (make-world)
        throttle 0.0]
    (testing (str "with throttle " throttle)
      (is (= 0.0 (predict-velocity world 0.0 throttle))))
    (let [throttle 0.65]
      (testing (str "with throttle " throttle)
        (is (near= 0.13 (predict-velocity world 0.0 throttle)))
        (is (near= 0.2574 (predict-velocity world 0.13 throttle)))
        (is (near= 0.382252 (predict-velocity world 0.2574 throttle)))))
    (let [throttle 1.0]
      (testing (str "with throttle " throttle)
        (is (near= 0.2 (predict-velocity world 0.0 throttle)))
        (is (near= 0.396 (predict-velocity world 0.2 throttle)))
        (is (near= 0.58808 (predict-velocity world 0.396 throttle)))))))

(deftest learn-acceleration-test
  (let [events [{:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.0, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc"}
                {:msgType "gameStart", :data nil, :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 0}
                {:gameTick 0, :data 1.0, :msgType "throttle"}]
        world (-> keimola-race
                  (as-> $ (reduce update-world $ events)))]
    (testing "default values"
      (is (near= 10.0 (max-velocity world 1.0)))
      (is (near= 0.396 (predict-velocity world 0.2 1.0))))
    (testing "detects actual values, case 1"
      (let [events [{:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.2, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 1}
                    {:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.5960000000000001, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 2}]
            world (reduce update-world world events)]
        (is (near= 10.0 (max-velocity world 1.0)))
        (is (near= 0.58808 (predict-velocity world 0.396 1.0)))))
    (testing "detects actual values, case 2"
      (let [events [{:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.18289316426093888, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "e5b0753a-6cd8-4f86-9af7-b9d2eaf42c14", :gameTick 1}
                    {:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.5452169620307792, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "e5b0753a-6cd8-4f86-9af7-b9d2eaf42c14", :gameTick 2}]
            world (reduce update-world world events)]
        (is (near= 9.660537892319338 (max-velocity world 1.0)))
        (is (near= 0.538357453109040 (predict-velocity world 0.362323797769840 1.0)))))))

(deftest learn-drifting-test
  (let [events [{:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 3, :inPieceDistance 96.37428037783354, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 82}
                {:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 4, :inPieceDistance 2.874280377833543, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 83}
                {:msgType "carPositions", :data [{:id me, :angle 0.18637081418040086, :piecePosition {:pieceIndex 4, :inPieceDistance 9.37428037783355, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 84}
                {:msgType "carPositions", :data [{:id me, :angle 0.5389610982579467, :piecePosition {:pieceIndex 4, :inPieceDistance 15.87428037783355, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 85}
                {:msgType "carPositions", :data [{:id me, :angle 1.0382841091847905, :piecePosition {:pieceIndex 4, :inPieceDistance 22.37428037783355, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "8ef465d8-0655-4654-8a88-fb0d7bd3a8cc", :gameTick 86}]
        world (-> keimola-race
                  (assoc-in [:physics :drifting-const1] "do-not-return-default-data")
                  (assoc-in [:physics :drifting-const2] "do-not-return-default-data")
                  (as-> $ (reduce update-world $ events)))
        history (car-history world)]
    (testing "detects drifting constants in the first turn"
      (is (near= 0.1 (drifting-const1 world)))
      (is (near= 800.0 (drifting-const2 world)))
      (is (near= 0.18637081418040086 (drifting-comp1 world history)))
      (is (near= -0.0539893006 (drifting-comp2 world history)))
      (is (near= -0.0043790589 (drifting-comp3 world history)))
      (is (near= 0.1280024547 (predict-drift-angle-dd world history))))
    (testing "interpolates comp1 for new velocities:"
      (testing "exact match - returns it"
        (is (= 30.0
               (-> (make-world)
                   (set-drifting-comp1 {:radius 100, :velocity 5.0} 30.0)
                   (get-drifting-comp1 {:radius 100, :velocity 5.0})))))
      (testing "only one data point - returns the closest one"
        (is (= 30.0
               (-> (make-world)
                   (set-drifting-comp1 {:radius 100, :velocity 5.0} 30.0)
                   (get-drifting-comp1 {:radius 100, :velocity 5.1})))))
      (testing "two data points - interpolates using them"
        (let [world (-> (make-world)
                        (set-drifting-comp1 {:radius 100, :velocity 5.0} 30.0)
                        (set-drifting-comp1 {:radius 100, :velocity 5.1} 40.0))]
          (is (near= 20.0 (get-drifting-comp1 world {:radius 100, :velocity 4.9})))
          (is (near= 35.0 (get-drifting-comp1 world {:radius 100, :velocity 5.05})))
          (is (near= 50.0 (get-drifting-comp1 world {:radius 100, :velocity 5.2})))))
      (testing "multiple data points - interpolates using the closest two"
        (let [world (-> (make-world)
                        (set-drifting-comp1 {:radius 100, :velocity 5.0} 10.0)
                        (set-drifting-comp1 {:radius 100, :velocity 5.1} 20.0)
                        (set-drifting-comp1 {:radius 100, :velocity 5.2} 40.0)
                        (set-drifting-comp1 {:radius 100, :velocity 5.3} 60.0))]
          (is (near= 5.0 (get-drifting-comp1 world {:radius 100, :velocity 4.95})))
          (is (near= 30.0 (get-drifting-comp1 world {:radius 100, :velocity 5.15})))
          (is (near= 70.0 (get-drifting-comp1 world {:radius 100, :velocity 5.35})))))
      (testing "does not mix data points from different radiuses"
        (let [world (-> (make-world)
                        (set-drifting-comp1 {:radius 100, :velocity 5.0} 10.0)
                        (set-drifting-comp1 {:radius 100, :velocity 5.1} 20.0)
                        (set-drifting-comp1 {:radius 90, :velocity 5.0} 40.0)
                        (set-drifting-comp1 {:radius 90, :velocity 5.1} 60.0))]
          (is (near= 15.0 (get-drifting-comp1 world {:radius 100, :velocity 5.05})))
          (is (near= 50.0 (get-drifting-comp1 world {:radius 90, :velocity 5.05})))))
      (testing "does not return negative comp1, but limits to 0.0"
        (let [world (-> (make-world)
                        (set-drifting-comp1 {:radius 100, :velocity 2.0} 10.0)
                        (set-drifting-comp1 {:radius 100, :velocity 3.0} 50.0))]
          (is (near= 0.0 (get-drifting-comp1 world {:radius 100, :velocity 1.0}))))))
    (testing "comp1 is saved positive"
      (let [world (-> keimola-race
                      (set-drifting-comp1 {:radius 110, :velocity 5.0} -10.0))]
        (is (near= 10.0 (get-drifting-comp1 world {:radius 110, :velocity 5.0})))
        (testing "and returned positive on right turns"
          (is (near= 10.0 (drifting-comp1 world [{:velocity 5.0, :piecePosition {:pieceIndex 4, :inPieceDistance 12.641152257602169, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}]))))
        (testing "but returned negative on left turns"
          (is (near= -10.0 (drifting-comp1 world [{:velocity 5.0, :piecePosition {:pieceIndex 14, :inPieceDistance 12.641152257602169, :lane {:startLaneIndex 1, :endLaneIndex 1}, :lap 0}}]))))))
    (testing "does not save comp1 for radius 0"
      (let [world (-> (make-world)
                      (set-drifting-comp1 {:radius 0, :velocity 5.0} 10.0))]
        (is (= 0.0 (get-drifting-comp1 world {:radius 0, :velocity 5.0})))))
    (testing "interpolates comp1 for new radiuses")         ; TODO
    (testing "when there are data points from multiple radiuses, prefers those from the same radius") ; TODO
    (testing "returns predefined constants before detecting the real data") ; TODO
    (testing "the predefined constants are removed once we have real data")) ; TODO
  (testing "velocity interpolation acceptance test"
    (let [events [; velocity 6.500000000000000
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 3, :inPieceDistance 99.64115225760216, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 83}
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 4, :inPieceDistance 6.141152257602172, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 84}
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.15885294306020667, :piecePosition {:pieceIndex 4, :inPieceDistance 12.641152257602169, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 85}
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.45938285471223117, :piecePosition {:pieceIndex 4, :inPieceDistance 19.141152257602172, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 86}
                  ; velocity 6.500000000000000
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.8849802325647208, :piecePosition {:pieceIndex 4, :inPieceDistance 25.641152257602172, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 87}
                  ; velocity 6.561499483757004
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 1.4196803513025769, :piecePosition {:pieceIndex 4, :inPieceDistance 32.202651741359176, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 88}
                  ; velocity 6.621677546985083
                   {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 2.0697639505603598, :piecePosition {:pieceIndex 4, :inPieceDistance 38.82432928834426, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 89}
                  ; velocity 6.680562582641514
                  #_{:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 2.839750087744241, :piecePosition {:pieceIndex 4, :inPieceDistance 45.50489187098577, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "aff447e0-c972-46da-8424-34c26bceb22d", :gameTick 90}]
          world (-> keimola-race
                    (as-> $ (reduce update-world $ events)))
          history (car-history world)]
      (is (near= 2.839750087744241
                 (predict-drift-angle world history)
                 0.001)))))

(deftest car-velocity-test
  (testing "when this and last tick on same piece"
    (is (= 6.991
           (-> keimola-race
               (update-world {:msgType "carPositions", :data [{:id me, :piecePosition {:pieceIndex 38, :inPieceDistance 88.048, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 2}}], :gameTick 2887})
               (update-world {:msgType "carPositions", :data [{:id me, :piecePosition {:pieceIndex 38, :inPieceDistance 95.039, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 2}}], :gameTick 2888})
               (car-velocity)))))
  (testing "when this and last tick on adjacent pieces"
    (is (= 6.991
           (-> keimola-race
               (update-world {:msgType "carPositions", :data [{:id me, :piecePosition {:pieceIndex 38, :inPieceDistance 95.039, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 2}}], :gameTick 2888})
               (update-world {:msgType "carPositions", :data [{:id me, :piecePosition {:pieceIndex 39, :inPieceDistance 2.030, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 2}}], :gameTick 2889})
               (car-velocity)))))
  (testing "when in a curve on track 0"
    (is (= 5.538797973719312
           (-> keimola-race
               (update-world {:msgType "carPositions", :data [{:id me, :piecePosition {:pieceIndex 4, :inPieceDistance 85.058, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameTick 129})
               (update-world {:msgType "carPositions", :data [{:id me, :piecePosition {:pieceIndex 5, :inPieceDistance 4.203, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameTick 130})
               (car-velocity)))))
  (testing "when in a curve on track 1"))                   ; TODO

(deftest car-history-test
  (let [pos1 {:pieceIndex 1, :inPieceDistance 0.123, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}
        pos2 {:pieceIndex 2, :inPieceDistance 0.123, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}]
    (testing "returns current and previous car positions"
      (let [history (-> keimola-race
                        (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 129})
                        (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos2}], :gameTick 130})
                        (car-history me))]
        (is (= {:id me, :piecePosition pos2}
               (-> history
                   first
                   (select-keys [:id :piecePosition]))))
        (is (= {:id me, :piecePosition pos1}
               (-> history
                   second
                   (select-keys [:id :piecePosition]))))))
    (testing "stores only the most recent car history (to save memory)"
      (is (= car-history-length
             (-> keimola-race
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 1})
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 2})
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 3})
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 4})
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 5})
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}], :gameTick 6})
                 car-history
                 count)))
      ; the above test assumes that it calls update-world enough many times to go over the limit
      (is (> 6 car-history-length)))
    (testing "identifies cars by ID"
      (is (= {:id "another player", :piecePosition pos2}
             (-> keimola-race
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}
                                                                {:id "another player", :piecePosition pos2}], :gameTick 129})
                 (car-history "another player")
                 first
                 (select-keys [:id :piecePosition])))))
    (testing "returns your car if ID not specified"
      (is (= {:id me, :piecePosition pos1}
             (-> keimola-race
                 (update-world {:msgType "carPositions", :data [{:id me, :piecePosition pos1}
                                                                {:id "another player", :piecePosition pos2}], :gameTick 129})
                 (car-history)
                 first
                 (select-keys [:id :piecePosition])))))
    (let [race-tick-0 (-> keimola-race
                          (update-world {:msgType "carPositions",
                                         :data    [{:id            me,
                                                    :piecePosition (assoc pos1 :inPieceDistance 0.0)
                                                    :angle         0.0}]}))
          race-tick-1 (-> race-tick-0
                          (update-world {:msgType "carPositions",
                                         :data    [{:id            me,
                                                    :piecePosition (assoc pos1 :inPieceDistance 0.2)
                                                    :angle         1.0}]}))
          race-tick-2 (-> race-tick-1
                          (update-world {:msgType "carPositions",
                                         :data    [{:id            me,
                                                    :piecePosition (assoc pos1 :inPieceDistance 0.55)
                                                    :angle         2.5}]}))]
      (testing "is enriched with car velocity"
        (is (near= 0.0 (-> race-tick-0 car-history first :velocity)))
        (is (near= 0.2 (-> race-tick-1 car-history first :velocity)))
        (is (near= 0.35 (-> race-tick-2 car-history first :velocity))))
      (testing "is enriched with car acceleration"
        (is (near= 0.0 (-> race-tick-0 car-history first :acceleration)))
        (is (near= 0.2 (-> race-tick-1 car-history first :acceleration)))
        (is (near= 0.15 (-> race-tick-2 car-history first :acceleration))))
      (testing "is enriched with car angle first derivative"
        (is (near= 0.0 (-> race-tick-0 car-history first :angle-d)))
        (is (near= 1.0 (-> race-tick-1 car-history first :angle-d)))
        (is (near= 1.5 (-> race-tick-2 car-history first :angle-d))))
      (testing "is enriched with car angle second derivative"
        (is (near= 0.0 (-> race-tick-0 car-history first :angle-dd)))
        (is (near= 1.0 (-> race-tick-1 car-history first :angle-dd)))
        (is (near= 0.5 (-> race-tick-2 car-history first :angle-dd))))))
  (testing "when history is empty, fills it with multiple copies of the first message"
    (let [history (-> keimola-race
                      (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.0, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "3cadf22e-350f-43e0-8fa2-d11ab6939b6e"})
                      (car-history))]
      (is (= car-history-length (count history)))
      (is (apply = history)))))

(deftest car-crash-test
  (let [before-crashed (-> keimola-race
                           (update-world {:gameTick 0, :data 1.0, :msgType "throttle"})
                           (update-world {:msgType "carPositions", :data [{:id me, :angle 58.71154252936955, :piecePosition {:pieceIndex 6, :inPieceDistance 43.45138760352983, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "0fa980db-3f0c-4372-9d84-aac84d1d1ca4", :gameTick 111}))
        after-crashed (-> before-crashed
                          (update-world {:msgType "crash", :data {:name "Kaali", :color "red"}, :gameId "0fa980db-3f0c-4372-9d84-aac84d1d1ca4", :gameTick 112})
                          (update-world {:msgType "carPositions", :data [{:id me, :angle 0.0, :piecePosition {:pieceIndex 6, :inPieceDistance 43.45138760352983, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "0fa980db-3f0c-4372-9d84-aac84d1d1ca4", :gameTick 112}))
        after-spawned (-> after-crashed
                          (update-world {:msgType "spawn", :data {:name "Kaali", :color "red"}, :gameId "0fa980db-3f0c-4372-9d84-aac84d1d1ca4", :gameTick 512})
                          (update-world {:msgType "carPositions", :data [{:id {:name "Kaali", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 6, :inPieceDistance 43.45138760352983, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "0fa980db-3f0c-4372-9d84-aac84d1d1ca4", :gameTick 512}))]
    (testing "at first cars are not crashed"
      (let [history (car-history before-crashed)]
        (is (= false (:crashed (first history))))))
    (testing "keeps track of when a car crashes"
      (let [history (car-history after-crashed)]
        (is (= true (:crashed (first history))))
        (testing "- old history is immutable"               ; the affected carPositions event comes after the crash event
          (is (= false (:crashed (second history)))))))
    (testing "keeps track of when a car spawns"
      (let [history (car-history after-spawned)]
        (is (= false (:crashed (first history))))
        (testing "- old history is immutable"               ; the affected carPositions event comes after the spawn event
          (is (= true (:crashed (second history)))))
        (testing "- the game sets throttle to 0 on spawn"
          (is (= 1.0 (:throttle before-crashed)))
          (is (= 1.0 (:throttle after-crashed)))
          (is (= 0.0 (:throttle after-spawned))))))))

(deftest get-lane-length-test
  (let [left-lane 0
        right-lane 1]
    ; left turn  = negative angle
    ; right turn = positive angle
    ; left lane  = negative distance from center line
    ; right lane = positive distance from center line

    (testing "straight piece"
      (is (= 100.0
             (-> (custom-track-race [{:length 100.0}])
                 (get-lane-length {:pieceIndex 0, :lane {:startLaneIndex left-lane
                                                         :endLaneIndex   left-lane}})))))
    (testing "right turn, outer/left lane"
      (is (= 86.39379797371932
             (-> (custom-track-race [{:radius 100, :angle 45.0}])
                 (get-lane-length {:pieceIndex 0, :lane {:startLaneIndex left-lane
                                                         :endLaneIndex   left-lane}})))))
    (testing "right turn, inner/right lane"
      (is (= 70.68583470577035
             (-> (custom-track-race [{:radius 100, :angle 45.0}])
                 (get-lane-length {:pieceIndex 0, :lane {:startLaneIndex right-lane
                                                         :endLaneIndex   right-lane}})))))
    (testing "left turn, outer/right lane"
      (is (= 86.39379797371932
             (-> (custom-track-race [{:radius 100, :angle -45.0}])
                 (get-lane-length {:pieceIndex 0, :lane {:startLaneIndex right-lane
                                                         :endLaneIndex   right-lane}})))))
    (testing "left turn, inner/left lane"
      (is (= 70.68583470577035
             (-> (custom-track-race [{:radius 100, :angle -45.0}])
                 (get-lane-length {:pieceIndex 0, :lane {:startLaneIndex left-lane
                                                         :endLaneIndex   left-lane}})))))))

(deftest get-lane-radius-test
  (let [left-lane 0
        right-lane 1]

    (testing "straight piece"
      (is (= 0.0
             (-> (custom-track-race [{:length 100.0}])
                 (get-lane-radius {:pieceIndex 0, :lane {:startLaneIndex left-lane
                                                         :endLaneIndex   left-lane}})))))
    (testing "right turn, outer/left lane"
      (is (= 110.0
             (-> (custom-track-race [{:radius 100, :angle 45.0}])
                 (get-lane-radius {:pieceIndex 0, :lane {:startLaneIndex left-lane
                                                         :endLaneIndex   left-lane}})))))
    (testing "right turn, inner/right lane"
      (is (= 90.0
             (-> (custom-track-race [{:radius 100, :angle 45.0}])
                 (get-lane-radius {:pieceIndex 0, :lane {:startLaneIndex right-lane
                                                         :endLaneIndex   right-lane}})))))
    (testing "left turn, outer/right lane"
      (is (= 110.0
             (-> (custom-track-race [{:radius 100, :angle -45.0}])
                 (get-lane-radius {:pieceIndex 0, :lane {:startLaneIndex right-lane
                                                         :endLaneIndex   right-lane}})))))
    (testing "left turn, inner/left lane"
      (is (= 90.0
             (-> (custom-track-race [{:radius 100, :angle -45.0}])
                 (get-lane-radius {:pieceIndex 0, :lane {:startLaneIndex left-lane
                                                         :endLaneIndex   left-lane}})))))))

(deftest update-world-test
  (testing "saves gameTick"
    (is (= 10 (:gameTick (-> (make-world)
                             (update-world {:gameTick 10}))))))
  (testing "keeps old gameTick if a message didn't have it"
    (is (= 10 (:gameTick (-> (make-world)
                             (update-world {:gameTick 10})
                             (update-world {})))))))
