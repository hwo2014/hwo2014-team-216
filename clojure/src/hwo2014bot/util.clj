(ns hwo2014bot.util)

(defn near=
  ([a b] (near= a b 0.0000001))
  ([a b delta]
   (cond
     (and (number? a) (number? b)) (> delta (Math/abs (- a b)))
     (or (number? a) (number? b)) false
     (and (coll? a) (empty? a)) (empty? b)
     (and (coll? b) (empty? b)) (empty? a)
     (and (map? a) (map? b)) (let [a (into (sorted-map) a)
                                   b (into (sorted-map) b)]
                               (and
                                 (= (keys a) (keys b))
                                 (recur (vals a) (vals b) delta)))
     (and (coll? a) (coll? b)) (and
                                 (near= (first a) (first b) delta)
                                 (recur (rest a) (rest b) delta))
     :else (= a b))))

(defn mode [numbers]
  (first (last (sort-by second (frequencies numbers)))))
