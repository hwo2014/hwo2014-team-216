(ns hwo2014bot.core
  (:require [hwo2014bot.network :as network]
            [hwo2014bot.util :refer [near= mode]]
            [clojure.pprint :refer [pprint]])
  (:gen-class))

(def ai-type (System/getenv "AITYPE"))

(def logging-levels (-> (or (System/getenv "LOGGING") "info")
                        (clojure.string/split #",")
                        (->> (filter #(not (.isEmpty %))))
                        (->> (map keyword))
                        (set)))

(defn log? [level]
  (contains? logging-levels level))

(defn warn [world message]
  (println (str "WARN: Tick" (:gameTick world) ": " message)))

; Observations

(def no-throttle 0.0)
(def full-throttle 1.0)
(def car-history-length 5)
(def crash-angle 50.0)                                      ; TODO: increase to 60.0 when predictions are accurate
(defn crash? [angle] (<= crash-angle (Math/abs angle)))

(defn game-started? [world]
  (boolean (:gameTick world)))

(defn make-world []
  {:throttle no-throttle
   :crashes  #{}})

(defn get-car-dimensions [world]
  (get-in world [:race :cars 0 :dimensions]))

(defn get-piece [world pieceIndex]
  (get-in world [:race :track :pieces pieceIndex]))

(defn get-lane-distance-from-center [world laneIndex]
  (:distanceFromCenter (first (filter #(= laneIndex (:index %))
                                      (get-in world [:race :track :lanes])))))

(defn get-lane-radius [world position]
  (let [piece (get-piece world (:pieceIndex position))
        lane-distance-from-center (get-lane-distance-from-center world (get-in position [:lane :startLaneIndex]))]
    (double (if (:angle piece)
              (if (< (:angle piece) 0)
                (+ (:radius piece) lane-distance-from-center)
                (- (:radius piece) lane-distance-from-center))
              0))))

(defn get-lane-length [world position]
  ; TODO: length of switch pieces (:startLaneIndex != :endLaneIndex)
  (let [piece (get-piece world (:pieceIndex position))
        lane-radius (get-lane-radius world position)]
    (double (or (:length piece)
                (/ (* Math/PI
                      lane-radius
                      (Math/abs (:angle piece)))
                   180.0)))))

(defn find-by-id [needle haystack]
  (let [matching-id? #(= (:id %) needle)]
    (first (filter matching-id? haystack))))

(defn car-history
  ([world] (car-history world (:yourCar world)))
  ([world carId] (map (partial find-by-id carId) (:carPositions world))))

(defn car-velocity
  ([world] (car-velocity world (car-history world)))
  ([world history] (let [prev-pos (get-in (second history) [:piecePosition])
                         curr-pos (get-in (first history) [:piecePosition])
                         prev-distance (:inPieceDistance prev-pos)
                         curr-distance (:inPieceDistance curr-pos)
                         curr-distance (if (= (:pieceIndex curr-pos) (:pieceIndex prev-pos))
                                         curr-distance
                                         (+ curr-distance (get-lane-length world prev-pos)))]
                     (- curr-distance prev-distance))))

(defn current-piece [world]
  (->> (get-in (first (car-history world)) [:piecePosition :pieceIndex])
       (get-piece world)))


; Track Model

(defn advance-track [velocity track]
  (cond
    (empty? track) track
    :else (let [piece (update-in (first track) [:length] - velocity)]
            (if (< 0.0 (:length piece))
              (cons piece (rest track))
              (recur (- (:length piece)) (rest track))))))


; Acceleration Physics

(defn max-velocity-ratio [world]
  (get-in world [:physics :max-velocity-ratio] 10.0))

(defn acceleration-ratio [world]
  (get-in world [:physics :acceleration-ratio] 50.0))


; Learning Acceleration

(defn learn-acceleration [world]
  (if (or (get-in world [:physics :i-know-acceleration])
          (not (:carPositions world)))
    world
    (let [history (car-history world)
          throttle (:throttle world)
          prev-velocity (:velocity (nth history 1))
          acceleration (:acceleration (nth history 0))
          prev-acceleration (:acceleration (nth history 1))]
      (if (and (< 0.0 acceleration prev-acceleration)
               (< 0.0 throttle))
        (let [acceleration-ratio (/ prev-acceleration
                                    (- prev-acceleration acceleration))

              max-velocity-ratio (/ (+ (* acceleration acceleration-ratio)
                                       prev-velocity)
                                    throttle)]
          (-> world
              (assoc-in [:physics :max-velocity-ratio] max-velocity-ratio)
              (assoc-in [:physics :acceleration-ratio] acceleration-ratio)
              (assoc-in [:physics :i-know-acceleration] true)))
        world))))


; Drift Physics

(defn drifting-const1 [world]
  (get-in world [:physics :drifting-const1] 0.1))

(defn drifting-const2 [world]
  (get-in world [:physics :drifting-const2] 800.0))

(defn drifting-comp3-var [world velocity]
  (- (/ velocity (drifting-const2 world))))

(defn drifting-comp3 [world history]
  (let [t0 (nth history 0)
        t1 (nth history 1)]
    (* (:angle t1)
       (drifting-comp3-var world (:velocity t0)))))

(defn drifting-comp2-var [world velocity]
  (- (drifting-comp3-var world velocity)
     (drifting-const1 world)))

(defn drifting-comp2 [world history]
  (let [t0 (nth history 0)
        t1 (nth history 1)
        angle-change (- (:angle t0)
                        (:angle t1))]
    (* angle-change (drifting-comp2-var world (:velocity t0)))))

(defn drifting-comp1-key [world history]
  (let [t0 (nth history 0)
        velocity (:velocity t0)
        radius (Math/round (get-lane-radius world (:piecePosition t0)))]
    {:radius radius, :velocity velocity}))

(defn set-drifting-comp1 [world key comp1]
  (if (= 0 (:radius key))
    world
    (assoc-in world [:physics :drifting-comp1 key] (Math/abs comp1))))

(defn distance-to-comp1-key [needle]
  (fn [[key val]]
    (Math/abs (- (:velocity key)
                 (:velocity needle)))))

(defn get-drifting-comp1 [world key]
  (let [data-points (->> (seq (get-in world [:physics :drifting-comp1]))
                         (filter #(= (:radius key)
                                     (:radius (first %))))
                         (sort-by (distance-to-comp1-key key))
                         (take 2))]
    (case (count data-points)
      0 0.0
      1 (second (first data-points))
      2 (let [entry0 (first data-points)
              entry1 (second data-points)
              key0 (first entry0)
              key1 (first entry1)
              val0 (second entry0)
              val1 (second entry1)]
          ; TODO: add some safety margin (e.g. 0.0003) when desired velocity is larger than all data points, to avoid crashing in corners?
          ; TODO: do quadratic interpolation?
          ; http://en.wikipedia.org/wiki/Linear_interpolation#Linear_interpolation_between_two_known_points
          (max 0.0
               (+ val0
                  (* (- val1 val0)
                     (/ (- (:velocity key)
                           (:velocity key0))
                        (- (:velocity key1)
                           (:velocity key0))))))))))

(defn drifting-comp1 [world history]
  (let [piece-index (get-in (first history) [:piecePosition :pieceIndex])
        turn-angle (or (:angle (get-piece world piece-index))
                       0)
        comp1 (get-drifting-comp1 world (drifting-comp1-key world history))]
    (if (< 0 turn-angle)
      comp1
      (- comp1))))

(defn predict-drift-angle-dd [world history]
  (+ (drifting-comp1 world history)
     (drifting-comp2 world history)
     (drifting-comp3 world history)))

(defn predict-drift-angle-d [world history]
  (let [angle-d (:angle-d (first history))
        angle-dd (predict-drift-angle-dd world history)]
    (+ angle-d angle-dd)))

(defn predict-drift-angle [world history]
  (let [angle (:angle (first history))
        angle-d (predict-drift-angle-d world history)]
    (+ angle angle-d)))


; Learning Drifting

(defn save-drifting-statistics [world]
  (let [history (car-history world)]
    (if (not (:crashed (first history)))                    ; when a car crashes, its angle is reset to 0, which would mess up our calculations
      (let [angle-dd (:angle-dd (first history))
            old-history (rest history)
            comp2 (drifting-comp2 world old-history)
            comp3 (drifting-comp3 world old-history)
            comp1 (- angle-dd comp2 comp3)]
        (set-drifting-comp1 world (drifting-comp1-key world old-history) comp1))
      world)))

(defn consequtive-car-velocities [history ticks]
  (map #(:velocity (nth history %))
       (range ticks)))

(defn learn-drifting [world]
  (cond
    (not (:carPositions world)) world                       ; history will be emptied between races, but physics not
    (get-in world [:physics :i-know-drifting]) (save-drifting-statistics world)
    :else (let [history (car-history world)
                tick-0 (nth history 3 nil)
                tick-1 (nth history 2 nil)
                tick-2 (nth history 1 nil)
                tick-3 (nth history 0 nil)]
            (if (and (= 0.0 (:angle tick-0))
                     (< 0.0 (:angle tick-1)))
              (let [velocities (consequtive-car-velocities history 4)
                    velocity (mode velocities)

                    comp1 (:angle tick-1)
                    comp2 (- (:angle-dd tick-2)
                             (:angle-dd tick-1))
                    comp2-var (/ comp2
                                 comp1)

                    comp1 (:angle tick-1)
                    comp2 (* (:angle-d tick-2)
                             comp2-var)
                    comp3 (:angle-dd tick-3)
                    comp3-var (/ (- comp3
                                    comp1
                                    comp2)
                                 comp1)

                    ; TODO: round these numbers to 10th decimal?
                    ; TODO: figure out why we sometimes have rounding errors, maybe due to velocity fluctuations?
                    const1 (- comp3-var comp2-var)
                    const2 (- (/ velocity comp3-var))]
                (when-not (near= (min velocities)
                                 (max velocities))
                  (warn world (str "Velocity not stable when learning to drift: " velocities)))
                (-> world
                    (assoc-in [:physics :drifting-const1] const1)
                    (assoc-in [:physics :drifting-const2] const2)
                    (assoc-in [:physics :i-know-drifting] true)
                    (recur)))                               ; recur to update statistics
              world))))


; Updates

(defn enrich-car [world new-car old-car]
  (let [velocity (if old-car
                   (car-velocity world [new-car old-car])
                   0.0)
        acceleration (- velocity
                        (or (:velocity old-car) 0.0))
        angle-d (- (or (:angle new-car) 0.0)
                   (or (:angle old-car) 0.0))
        angle-dd (- angle-d
                    (or (:angle-d old-car) 0.0))
        crashed (contains? (:crashes world)
                           (:id new-car))]
    (assoc new-car :velocity velocity
                   :acceleration acceleration
                   :angle-d angle-d
                   :angle-dd angle-dd
                   :crashed crashed)))

(defn enrich-cars [world new-cars old-cars]
  (map #(enrich-car world % (find-by-id (:id %) old-cars)) new-cars))

(def end-of-world (atom nil))

(defn- save-game-tick [world msg]
  (if (:gameTick msg)
    (assoc world :gameTick (:gameTick msg))
    world))

(defn update-world [world msg]
  (-> (case (:msgType msg)
        "gameInit" (merge world (:data msg))
        "yourCar" (assoc world :yourCar (:data msg))
        "carPositions" (let [enriched (enrich-cars world (:data msg) (first (:carPositions world)))
                             carPositions (if (empty? (:carPositions world))
                                            (repeat enriched)
                                            (cons enriched (:carPositions world)))]
                         (assoc world :carPositions (vec (take car-history-length carPositions)))) ; free memory by creating a non-lazy vector
        "crash" (update-in world [:crashes] conj (:data msg))
        "spawn" (as-> world world
                      (update-in world [:crashes] disj (:data msg))
                      (if (= (:yourCar world) (:data msg))
                        (assoc world :throttle 0.0)
                        world))
        "turboAvailable" (assoc world :turboAvailable (:data msg))
        "throttle" (assoc world :throttle (:data msg))
        "gameEnd" (do
                    (reset! end-of-world world)             ; save the world before removing transient data
                    (merge (make-world)                     ; remove all but auto-detected physics variables
                           (select-keys world [:yourCar :physics]))) ; TODO: save physics variables under :physics
        "tournamentEnd" (pprint @end-of-world)              ; must be done last, because CI would timeout otherwise
        world)
      (save-game-tick msg)
      (learn-acceleration)
      (learn-drifting)))


; Predictions

(defn max-velocity [world throttle]
  (* throttle (max-velocity-ratio world)))

(defn predict-acceleration [world velocity throttle]
  (/ (- (max-velocity world throttle)
        velocity)
     (acceleration-ratio world)))

(defn predict-velocity [world velocity throttle]
  (+ velocity
     (predict-acceleration world velocity throttle)))

(defn- to-throttle-range [throttle]
  (max no-throttle (min full-throttle throttle)))

(defn predict-throttle [world velocity speed-limit]
  (let [acceleration (- speed-limit velocity)]
    (to-throttle-range (/ (+ velocity (* acceleration (acceleration-ratio world)))
                          (max-velocity-ratio world)))))

(defn predict-throttles
  ([world ticks velocity track] (reverse (predict-throttles world ticks velocity track nil)))
  ([world ticks velocity track throttles]
   (if (= 0 ticks)
     throttles
     (let [speed-limit (:speed-limit (first track))
           throttle (predict-throttle world velocity speed-limit)
           velocity (predict-velocity world velocity throttle)
           track (advance-track velocity track)]
       (recur world (dec ticks) velocity track (cons throttle throttles))))))

(defn simulate-car [world history]
  (let [car (first history)
        id (:id car)
        ; TODO: predict other cars' velocity
        ; TODO: predict bumping to other cars
        ; TODO: predict crashes (return a list of messages instead of just one)
        velocity (if (= id (:yourCar world))
                   (predict-velocity world (:velocity car) (:throttle world))
                   0.0)
        angle (predict-drift-angle world history)
        lane-length (get-lane-length world (:piecePosition car))
        pieces-in-track (count (get-in world [:race :track :pieces]))
        inPieceDistance (+ (get-in car [:piecePosition :inPieceDistance])
                           velocity)
        end-of-piece? (< lane-length inPieceDistance)
        inPieceDistance (- inPieceDistance
                           (if end-of-piece? lane-length 0.0))
        pieceIndex (+ (get-in car [:piecePosition :pieceIndex])
                      (if end-of-piece? 1 0))
        end-of-track? (>= pieceIndex pieces-in-track)
        pieceIndex (- pieceIndex
                      (if end-of-track? pieces-in-track 0))
        lap (+ (get-in car [:piecePosition :lap])
               (if end-of-track? 1 0))]
    {:id            id,
     :angle         angle,
     :piecePosition {:pieceIndex      pieceIndex,
                     :inPieceDistance inPieceDistance,
                     ; TODO: predict lane switch
                     :lane            {:startLaneIndex (get-in car [:piecePosition :lane :startLaneIndex]),
                                       :endLaneIndex   (get-in car [:piecePosition :lane :endLaneIndex])},
                     :lap             lap}}))

(defn find-crashes [carPositions]
  (->> carPositions
       (filter #(crash? (:angle %)))
       (map :id)))

(defn simulate-server-response [world]
  (let [ids (map :id (first (:carPositions world)))
        carPositions (map #(simulate-car world (car-history world %)) ids)
        crashes (set (find-crashes carPositions))
        carPositions (map #(if (contains? crashes (:id %))
                            (-> (car-history world (:id %))
                                (first)
                                (select-keys [:id :angle :piecePosition])
                                (assoc :angle 0.0))
                            %) carPositions)
        gameTick (inc (:gameTick world))]
    (concat (map #(-> {:msgType  "crash",
                       :data     %,
                       :gameTick gameTick})
                 crashes)
            [{:msgType  "carPositions",
              :data     (vec carPositions),
              :gameTick gameTick}])))

(defn simulate-world [world ticks]
  (if (>= 0 ticks)
    world
    (recur (reduce update-world world (simulate-server-response world))
           (dec ticks))))


; AI

(defn drive-with-throttle [world ticks throttle]
  (-> world
      (update-world {:msgType "throttle", :data throttle})
      (simulate-world ticks)))

(defn can-avoid-crashing? [world ticks]
  (let [world (drive-with-throttle world ticks 0.0)
        car (first (car-history world))]
    (not (:crashed car))))

(defn largest-safe-throttle [world]                         ; TODO: binary search for more optimal throttle
  (let [world (drive-with-throttle world 1 1.0)]
    (if (can-avoid-crashing? world 50)
      1.0
      0.0)))

(defmulti decide-next-action :msgType)

(defmethod decide-next-action "gameStart" [msg world]
  {:msgType "throttle", :data 1.0})

(defmethod decide-next-action "carPositions" [msg world]
  ; car angle going over 60 will cause a crash
  ; constant throttle 0.65 is safe, but 0.66 will result in crashes
  (cond
    (not (game-started? world)) nil
    :else (case ai-type
            "A" (rand-nth [{:msgType "throttle", :data 0.1}
                           {:msgType "switchLane", :data "Left"}
                           #_{:msgType "switchLane", :data "Right"}])
            "B" (rand-nth [{:msgType "throttle", :data 0.65}
                           {:msgType "switchLane", :data "Left"}
                           #_{:msgType "switchLane", :data "Right"}])
            #_(if (< (get-in (car-positions world) [:current :piecePosition :pieceIndex])
                   6)
              {:msgType "throttle", :data (predict-throttle world (car-velocity world) 6.0)}
              {:msgType "throttle", :data (predict-throttle world (car-velocity world) 0.0)})
            #_(if (< 80 (:gameTick world))
              {:msgType "throttle", :data 0.65}
              {:msgType "throttle", :data (predict-throttle world (car-velocity world) 6.5)})
            #_(if (get-in world [:physics :i-know-drifting])
              {:msgType "throttle", :data (predict-throttle world (:velocity (first (car-history world))) 10.0)}
              {:msgType "throttle", :data (predict-throttle world (:velocity (first (car-history world))) 6.5)})
            {:msgType "throttle", :data (largest-safe-throttle world)})))

(defmethod decide-next-action :default [msg world]
  nil)


; Main

(defn info-logging [message]
  (case (:msgType message)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "turboAvailable" (println "Turbo available")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data message)))
    :noop))

(defn debug-logging [world message]
  (if (and (game-started? world)
           (= "carPositions" (:msgType message)))
    (let [history (car-history world)
          car (first history)
          piece (get-piece world (get-in car [:piecePosition :pieceIndex]))]
      (println (format (str "velocity\t%.15f\t"
                            "throttle\t%.15f\t"
                            "carAngle\t%.15f\t"
                            "pieceIndex\t%d\t"
                            "pieceAngle\t%.15f\t"
                            "laneRadius\t%.15f\t"
                            "inPieceDistance\t%.15f\t"
                            "predictedAngleChange2\t%.15f\t")
                       (:velocity car)
                       (:throttle world)
                       (:angle car)
                       (:pieceIndex (:piecePosition car))
                       (or (:angle piece) 0.0)
                       (get-lane-radius world (:piecePosition car))
                       (:inPieceDistance (:piecePosition car))
                       (predict-drift-angle-dd world history)))
      #_(prn {:velocity               velocity
            :predictedVelocity      (:predictedVelocity world)
            :predictedVelocityError (- (:predictedVelocity world) velocity)
            :throttle               (:throttle world)
            :carAngle               (:angle position)
            :pieceIndex             (:pieceIndex (:piecePosition position))
            :piece                  (get-piece world (:pieceIndex (:piecePosition position)))}))))

(defn update-predictions [world]
  (assoc world :predictedVelocity (predict-velocity world
                                                    (:velocity (first (car-history world)))
                                                    (:throttle world))))

(defn game-loop [channel world]
  (let [message (network/read-message channel)
        world (update-world world message)]
    (when (log? :trace)
      (prn :<- message))
    (when (log? :info)
      (info-logging message))
    (when (log? :debug)
      (debug-logging world message))

    (let [command (-> (decide-next-action message world)
                      (assoc :gameTick (:gameTick world)))]
      (if (:msgType command)
        (do
          (when (log? :trace)
            (prn :-> command))
          (network/send-message channel command)
          (recur channel (-> world
                             (update-world command)
                             (update-predictions))))
        (recur channel world)))))

(defn -main [& [host port botname botkey]]
  (let [channel (network/connect-client-channel host (Integer/parseInt port))]
    (network/send-message channel
                          (case ai-type
                            "A" {:msgType "createRace"
                                 :data    {:botId     {:name (str botname ai-type) :key botkey}
                                           :trackName "keimola"
                                           :password  "woot"
                                           :carCount  2}}
                            "B" {:msgType "joinRace"
                                 :data    {:botId     {:name (str botname ai-type) :key botkey}
                                           :trackName "keimola"
                                           :password  "woot"
                                           :carCount  2}}
                            "germany" {:msgType "createRace"
                                       :data    {:botId     {:name botname :key botkey}
                                                 :trackName "germany"
                                                 :password  "woot"
                                                 :carCount  1}}
                            {:msgType "join"
                             :data    {:name botname :key botkey}}))
    (game-loop channel (make-world))))
